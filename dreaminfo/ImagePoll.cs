﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace dreaminfo
{

    struct Data {
        public int index { get; set; }
        public Image subject_image { get; set; }
        public Image output_image { get; set; }
        public string desc { get; set; }
        public Data(int index, Image subject, Image output, string desc)
        {
            this.index = index;
            this.subject_image = subject;
            this.output_image = output;
            this.desc = desc;
        }
        public Data(Data data)
        {
            this.index = data.index;
            this.subject_image = data.subject_image;
            this.output_image = data.output_image;
            this.desc = data.desc;
        }
    };

    class ImagePoll{
        private List<Data> item;
        private static ImagePoll instance;

        public ImagePoll()
        {
        }

        public static ImagePoll getInstance() {
            if(instance == null) {
                instance = new ImagePoll();
                instance.item = new List<Data>();
                for (int i = 0; i < 9; i++) {
                    instance.item.Add(new Data(i, null, null, null));
                }
            }
            return instance;
        }

        public void insert(int index, Image sub, Image output, string desc)
        {
            string dir = @"c:\temp";
            string serializationFile = Path.Combine(dir, "salesmen.bin");
            //serialize
            using (Stream stream = File.Open(serializationFile, FileMode.Create))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                bformatter.Serialize(stream, item);
            }

            //deserialize
            using (Stream stream = File.Open(serializationFile, FileMode.Open))
            {
                var bformatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                List<Data> salesman = (List<Data>)bformatter.Deserialize(stream);
            }
        }

        public void delete(int index)
        {
               
        }
    }
}
